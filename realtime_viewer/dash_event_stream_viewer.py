"""placeholder."""
from typing import Any
import plotly.graph_objects as go
from dash.dependencies import Input, Output, State
from dash import dcc, html, Dash
import dash_bootstrap_components as dbc
from PIL import Image
import io

import requests
from imageio import v3 as iio

IMAGE_SERVER_URL = "http://127.0.0.1:8000/image"
IMAGE_SERVER_STREAM_URL = "http://127.0.0.1:8000/image_event_stream"
app = Dash()


def get_image_from_server() -> io.BytesIO:
    response = requests.get(IMAGE_SERVER_URL)
    if response.status_code != 200:
        raise RuntimeError("something happened")
    return io.BytesIO(response.content)


def get_image() -> Image.Image:
    img = get_image_from_server()
    return Image.open(img)


app.layout = html.Div(
    [
        html.Div(
            className="row",
            children=[
                dcc.Store(id="session_store", storage_type="session"),
                dbc.Button("start session", id="start_session", n_clicks=0),
                dbc.Button("end session", id="end_session", n_clicks=0),
                html.Img(id="image_element", src=get_image()),
                dcc.Interval(
                    id="interval_component",
                    interval=0.5 * 1000,  # in milliseconds
                    n_intervals=0,
                ),
            ],
        ),
    ]
)


app.clientside_callback(
    f"""
    async function(n_clicks) {{
        if (n_clicks < 1) {{
            return {{}};
        }}
        data = {{}};
        const evtSource = new EventSource('{IMAGE_SERVER_STREAM_URL}');
        data['event_stream'] = evtSource;
        return data
    }}
    """,
    Output("session_store", "data"),
    Input("start_session", "n_clicks"),
)


if __name__ == "__main__":
    app.run_server(debug=True)
