"""placeholder."""
import numpy as np


def moving_square_frames(
    square_center: tuple[int, int] = (50, 50),
    grid_size: int = 100,
    square_size: int = 3,
    move_rate: int = 1,
):
    """placeholder."""
    while True:
        square_center = (square_center[0], (square_center[1] + move_rate) % grid_size)
        img = np.zeros([grid_size, grid_size, 3], dtype=np.uint8)
        img.fill(255)
        if square_size > 0:
            img[
                square_center[0] - square_size : square_center[0] + square_size,
                square_center[1] - square_size : square_center[1] + square_size,
            ].fill(0)
        yield img
