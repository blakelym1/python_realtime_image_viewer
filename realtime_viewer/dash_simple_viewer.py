"""placeholder."""
import plotly.graph_objects as go
from dash.dependencies import Input, Output
from dash import dcc, html, Dash
import dash_bootstrap_components as dbc
from images import moving_square_frames

app = Dash()

gen_image = moving_square_frames()


def get_image() -> go.Figure:
    img = next(gen_image)
    return go.Figure(go.Image(z=img))


app.layout = html.Div(
    [
        html.Div(
            className="row",
            children=[
                dbc.Button("Change image", id="image_button", n_clicks=0),
                html.Div(dcc.Graph(id="image", figure=get_image())),
                dcc.Interval(
                    id="interval_component",
                    interval=0.5 * 1000,  # in milliseconds
                    n_intervals=0,
                ),
            ],
        ),
    ]
)


@app.callback(
    Output("image", "figure"),
    Input("interval_component", "n_intervals"),
)
def update_image_automatic(n_intervals):
    return get_image()


if __name__ == "__main__":
    app.run_server(debug=True)
