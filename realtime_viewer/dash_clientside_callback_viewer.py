"""placeholder."""
from typing import Any
import plotly.graph_objects as go
from dash.dependencies import Input, Output
from dash import dcc, html, Dash
import dash_bootstrap_components as dbc
from PIL import Image
import io

import requests
from imageio import v3 as iio


IMAGE_SERVER_URL = "http://127.0.0.1:8000/image"
app = Dash(prevent_initial_callbacks=True)


def get_image_from_server() -> io.BytesIO:
    response = requests.get(IMAGE_SERVER_URL)
    if response.status_code != 200:
        raise RuntimeError("something happened")
    return io.BytesIO(response.content)


def get_image() -> Image.Image:
    img = get_image_from_server()
    return Image.open(img)


app.layout = html.Div(
    [
        html.Div(
            className="row",
            children=[
                html.Img(id="image_element", src=get_image()),
                dcc.Interval(
                    id="interval_component",
                    interval=0.5 * 1000,  # in milliseconds
                    n_intervals=0,
                ),
            ],
        ),
    ]
)


app.clientside_callback(
    f"""
    async function(n_intervals, old_src) {{
        if (n_intervals > 1) {{
            URL.revokeObjectURL(old_src);
        }}
        const response = await fetch('{IMAGE_SERVER_URL}')
        const image_data = await response.blob();
        return URL.createObjectURL(image_data);
    }}
    """,
    Output("image_element", "src"),
    Input("interval_component", "n_intervals"),
    Input("image_element", "src"),
)


if __name__ == "__main__":
    app.run_server(debug=True)
