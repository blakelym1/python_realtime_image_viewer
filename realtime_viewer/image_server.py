from contextlib import asynccontextmanager
from realtime_viewer.images import moving_square_frames
import io
import asyncio
from fastapi import FastAPI
from sse_starlette.sse import EventSourceResponse
from imageio import v3 as iio
from fastapi import Response, Request
from fastapi.middleware.cors import CORSMiddleware
from logging import getLogger
import base64

logger = getLogger("image_server")
IMAGE_GENERATOR = (None for _ in range(10))

origins = [
    "http://localhost",
    "http://localhost:8000",
    "http://localhost:8050",
    "http://127.0.0.1",
    "http://127.0.0.1:8000",
    "http://127.0.0.1:8050",
]


@asynccontextmanager
async def lifespan(app: FastAPI):
    global IMAGE_GENERATOR
    IMAGE_GENERATOR = moving_square_frames()
    yield


app = FastAPI(lifespan=lifespan)

app.add_middleware(
    CORSMiddleware,
    allow_origins=origins,
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)


@app.get("/")
def read_root() -> dict[str, str]:
    return {"Hello": "World"}


@app.get("/image", response_class=Response)
def get_image() -> Response:
    im = next(IMAGE_GENERATOR)
    with io.BytesIO() as buf:
        iio.imwrite(buf, im, plugin="pillow", format="JPEG")
        im_bytes = buf.getvalue()

    headers = {"Content-Disposition": 'inline; filename="test.jpeg"'}
    return Response(im_bytes, headers=headers, media_type="image/jpeg")


async def image_event_generator(
    request: Request, event_delay: float, retry_timeout_ms: float = 30000
):
    while True:
        if await request.is_disconnected():
            logger.debug("Request disconnected")
            break

        for i, image in enumerate(IMAGE_GENERATOR):
            with io.BytesIO() as buf:
                iio.imwrite(buf, image, plugin="pillow", format="JPEG")
                im_base64 = base64.b64encode(buf.getvalue())

            yield {
                "event": "new_image",
                "id": str(i),
                "retry": retry_timeout_ms,
                "data": im_base64,
            }

            await asyncio.sleep(event_delay)


@app.get("/image_event_stream/", response_class=Response)
async def get_image_event_stream(
    request: Request, event_delay: float = 0.5
) -> EventSourceResponse:
    return EventSourceResponse(
        image_event_generator(request, event_delay), media_type="image/jpeg"
    )
