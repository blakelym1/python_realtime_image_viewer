from types import GeneratorType
from realtime_viewer.images import moving_square_frames
import matplotlib.pyplot as plt
import numpy as np


def test_moving_square_generates_ndarrays():
    expected_grid_size = 10
    image = next(moving_square_frames(grid_size=expected_grid_size))
    assert image.shape == (expected_grid_size, expected_grid_size, 3)


def test_moving_square_works_with_imshow():
    image_gen = moving_square_frames()
    assert isinstance(image_gen, GeneratorType)
    assert plt.imshow(next(image_gen)) is not None
