#!/bin/bash
SSH_PATH="/home/$(id -u -n)/.ssh"
cp /run/secrets/private_git_lab_key $SSH_PATH/private_git_lab_key
chmod 600 $SSH_PATH/private_git_lab_key
echo -e "# GitLab.com\nHost gitlab.com\n\tPreferredAuthentications publickey\n\tIdentityFile $HOME/.ssh/private_git_lab_key" > $SSH_PATH/config